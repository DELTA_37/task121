#include "integ.h"

double epsilon(double a)
{
	double e=1;
	while (a+e>a)
	{
		e=e/2;
	}
	return 2*e;
}

double integral(double (*f)(double),double a,double b, int n)
{
	int i;
	double s=0;
	double d=(b-a)/n;
	if (d<epsilon(a))
	{
		d=epsilon(a);
	}
	n=floor((b-a)/d);
	for (i=0;i<=n-1;i++)
	{
		s+=d*(f(a+i*d)+f(a+(i+1)*d))/2;
	}
	return s;
}


