#include "integ.h"
#include "func.h"

int main(int argc, char *argv[])
{
	if (argc==1)
	{
		FILE *fp;
		char s[100];
		double a,b;
		int n;
		scanf("%s",s);
		fp=fopen(s,"r");
		fscanf(fp,"%lf%lf%d",&a,&b,&n);
		printf("%e",integral(f,a,b,n));
	}
	if (argc==2)
	{
		FILE *fp;
		double a,b,e;
		fp=fopen(argv[1],"r");
		fscanf(fp,"%lf%lf%lf",&a,&b,&e);
		printf("%e",integral(f,a,b,(b-a)/(e*e)));
	}
	return 0;
}
