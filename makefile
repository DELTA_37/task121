all: prog
prog: main.o func.o integ.o
	gcc main.o func.o integ.o -g -lm -o prog
main.o: main.c func.h integ.h
	gcc main.c -c -lm -g
func.o: func.c func.h
	gcc func.c -c -lm -g
integ.o: integ.c integ.h
	gcc integ.c -c -lm -g
clean: 
	rm prog
update:
	touch main.c
	touch integ.c
	touch func.c
start:
	./gen in
	./prog	
gen: gen.c
	gcc gen.c -g -o gen

